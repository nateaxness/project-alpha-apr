from django.urls import path
from tasks.views import show_project
from projects.views import (
    create_project,
    project_list,
)

urlpatterns = [
    path("<int:id>/", show_project, name="show_project"),
    path("", project_list, name="list_projects"),
    path("create/", create_project, name="create_project"),
]
